from .models import list
from django import forms

class listForm(forms.ModelForm):
    class Meta:
        model = list
        fields=["item","completed"]